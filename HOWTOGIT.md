## So you want to implement a feature?

1. First check that you are on master:

    git status

2. If you are not on master, get on master:

    git checkout master

3. Get the latest master updates:

    git pull

4. Make your new branch:

    git branch mynewfeature

5. Switch to your branch:

    git checkout mynewfeature

6. do shit:

    vim mynewfeature.py
    vim mynewfeature.aiml
    pylint mynewfeature.py | less

7. commit changes:

    git add .
    git commit -m "I am the code God."

8. Push your branch and its changes.

    git push mynewfeature

9. Switch back to master.

    git checkout master

10. Repeat and profit.

