#!/usr/bin/python
"""
Grand Unified Server.

Component   |   Port
=====================
Speech      |   1001
AI          |   2002
Avatar      |   3003

"""
import sys
import errno
import socket

from myserv import MyServerSocket
from mysock import MySocket

def reconnect(conn, conn_name):
    sys.stdout.write(
      "connection to %s broken, waiting for reconnect..."
      % (conn_name))
    sys.stdout.flush()
    while not(conn.accept()): pass
    sys.stdout.write("...connection reestablished.\n")

def xfer(recv_conn, send_conn, recv_name, send_name):
    # tmp carries data from one server to the next
    tmp = ""

    # recv some data
    data_recved = False
    while not(data_recved):
        try:
            tmp = recv_conn.myrecv()
            data_recved = True

        except socket.error as err:
            if err[0] == errno.EPIPE: reconnect(recv_conn, recv_name)

    # send some data
    if tmp != "":
        try:
            send_conn.mysend(tmp)
            print "%s --> %s: %s" % (recv_name, send_name, repr(tmp))
        except socket.error as err:
            if err[0] == errno.EPIPE or err[0] == errno.ECONNRESET:
                reconnect(send_conn, send_name)


def accept(conn_flag, ss, msg):
    if not(conn_flag):
        conn_flag = ss.accept()
        if conn_flag: print msg
    return conn_flag


if __name__ == "__main__":
    print "clients must connect to the fqdn"
    print "speech:1001, AI:2002, avatar:3003"
    try:
        # ini server sockets
        speech_ss = MyServerSocket(socket.getfqdn(), 1001)
        ai_ss = MyServerSocket(socket.getfqdn(), 2002)
        avatar_ss = MyServerSocket(socket.getfqdn(), 3003)

        # accept server conns
        print "waiting for connections"
        speech_conn = ai_conn = avatar_conn = False
        while not(speech_conn) or not(ai_conn) or not(avatar_conn):
            speech_conn = accept(speech_conn, speech_ss, "speech connected")
            ai_conn = accept(ai_conn, ai_ss, "ai connected")
            avatar_conn = accept(avatar_conn, avatar_ss, "avatar connected")

        # xfer loop
        while True:
            xfer(speech_ss, ai_ss, "speech", "ai")
            xfer(ai_ss, avatar_ss, "ai", "avatar")

    except socket.error as err:
        if err[0] == errno.EACCES:
            sys.exit("Error: need to run with root permissions.")
        else: raise err

    except KeyboardInterrupt:
        speech_ss.close()
        ai_ss.close()
        avatar_ss.close()
        print ""
        print "Goodbye, cruel world."

