#!/usr/bin/python
import sys

import masterbot
from fifonet import mysock

if __name__ == "__main__":
    usage = "run.py host port"
    if len(sys.argv) != 3: sys.exit(usage)

    # get cmd line args
    host = sys.argv[1]
    port = int(sys.argv[2])

    # do stuff
    sock = mysock.MySocket()
    sock.connect(host, port)
    bot = masterbot.MasterBot()

    # this is a loop
    while True:
        query = sock.myrecv()

        if query != "":
            resp = bot.respond(query)
            sock.mysend(resp + "\n")
            print "query: " + query
            print "resp: " + resp

