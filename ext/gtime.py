#!/usr/bin/python
import json
import requests
import sys

url = "http://api.aerisapi.com/observations/%s?client_id=%s&client_secret=%s"

client_id = "wNqk1Xks2XPVwF3a9zOwx"
client_secret = "nfcGKtQVOJCSP8JdeIkLlmVLoik8BfZkSVQ4BqPb"

def parse_place(raw_place):
    """ Parse place string if not auto. """
    place = raw_place.strip().lower()
    if place != ":auto":
        parts = place.split(',')
        if len(parts) == 1:
            # has no commas
            parts = place.split(' ')
            half = len(parts)/2
            place = ""
            place = "+".join(parts[0:half]) + ',' + "+".join(parts[half:])
    place = place.replace(" ", "+")
    return place

def gtime(raw_place):
    place = parse_place(raw_place)
    r = requests.get(url % (place, client_id, client_secret)).json()

    # who want to know the time in indian anyways...
    if r['success']:
        utc = r['response']['obDateTime'][19:]
        if utc[1]: utc = utc[0] + utc[2] # TODO: .5 offsets
        return "time " + utc
    else:
        return "Please be more specific with your place."

if __name__ == "__main__":
    print time(" ".join(sys.argv[1:]))

