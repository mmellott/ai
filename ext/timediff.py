#!/usr/bin/python
################################################################################
# FILE: timediff.py
# AUTHOR: Greg Humphrey (greg.humphrey@asu.edu)
# DESCRIPTION:
#	The fn timediff returns the the number of days between the current day
#       and a specified date by  sending a GET request to some website and
#       scraping the results.
#
################################################################################
import datetime
import re
import requests # "urllib2 sucks giant horse...balloons." - Matthew Mellott
import sys
from bs4 import BeautifulSoup

# this is the unit conversion server I'm abusing
url = "http://calendarhome.com/cgi-bin/date2.pl?"

# returns time difference
def timediff(date):
        # get current date
        now = datetime.datetime.now()

        # get 'from' month
        month1 = "month1=%d" % (now.month)

        #get 'from' date
        date1 = "&date1=%d" % (now.day)

        # get 'from' year'
        year1 = "&year1=%d" % (now.year)

	# split date into parts
        parts = date.split()

        # get 'to' month
        month = parts[0].lower()

        if month == "january":
            month2 = "&month2=1"
        elif month == "february":
            month2 = "&month2=2"
        elif month == "march":
            month2 = "&month2=3"
        elif month == "april":
            month2 = "&month2=4"
        elif month == "may":
            month2 = "&month2=5"
        elif month == "june":
            month2 = "&month2=6"
        elif month == "july":
            month2 = "&month2=7"
        elif month == "august":
            month2 = "&month2=8"
        elif month == "september":
            month2 = "&month2=9"
        elif month == "october":
            month2 = "&month2=10"
        elif month == "november":
            month2 = "&month2=11"
        elif month == "december":
            month2 = "&month2=12"
        else:
            month2 = ""

        if month2 != "" :
            if len(parts) >= 2:
                # get 'to' date
                date2 = "&date2=" + parts[1].replace(",", "")
            else:
                # set date to the first of the month
                date2 = "&date2=1"

            if len(parts) >= 3:
                # get 'to' year
                year2 = "&year2=" + parts[2]
            else:
                # set year to the current one
                year2 = "&year2=%d" % (now.year)

            # concate query string
            querystring = month1 + date1 + year1 + month2 + date2 + year2
        
            soup = BeautifulSoup(requests.get(url + querystring).text)
            days = soup.find("h2")

            if days:
                return days.text
            else:
                return ""
        else:        
            return ""


# run module from console
if __name__ == "__main__":
	if len(sys.argv) != 2:
		print "usage: " + sys.argv[0] + " \"June 8 2045\""
		sys.exit(0)
	else:
		date = sys.argv[1]
		print timediff(date)
