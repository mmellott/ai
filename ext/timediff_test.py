#!/usr/bin/python
import aiml
import timediff

# setup bot
k = aiml.Kernel()
k.learn("./myaiml/timediff.aiml")

# verbose: displays error messages to console
k.verbose(True)

while True:
	query = raw_input("> ")
        #print "query: " + query 
	resp = k.respond(query)
        #print "resp: " + resp
        if resp != "":	
           print "resp: " + timediff.timediff(resp)

