#!/usr/bin/python
import requests # urllib2 sucks giant horse...balloons.
import sys
from bs4 import BeautifulSoup
import re

# link to POTUS list, looks relatively easy to parse
url = "http://home.comcast.net/~sharonday7/Presidents/AP060301.htm"
        
# returns the ith president of the United States
def pres(number):
        numberMatchObj = re.match('[0-9]+', number)
        if numberMatchObj is None:
            if number == "first":
                number = "1st"
            if number == "second":
                number = "2nd"
            if number == "third":
                number = "3rd"
            if number == "fourth":
                number = "4th" 
            if number == "fifth":
                number = "5th"
            if number == "sixth":
                number = "6th"
            if number == "seventh":
                number = "7th"
            if number == "eight":
                number = "8th" 
            if number == "ninth":
                number = "9th"
            if number == "tenth":
                number = "10th"
    
        # rerun number match    
        numberMatchObj = re.match('[0-9]+', number)
        if numberMatchObj is None:
            return ""        

        number = numberMatchObj.group()

        # get the presidents page and turn it into soup
        page = requests.get(url).text
        soup = BeautifulSoup(page)

        soup = soup.find_all("table")[3]
        
        # president wrapper. if something breaks, it is probably here
        filtered = soup.find_all(text = re.compile("^" + number + "[.] *"))

        # not a valid president index
        if len(filtered) == 0:
            return ""

        # remove the stupid number at begining
        index = filtered[0].find(" ")
        mr_president = filtered[0][index+1:] 

        if len(mr_president): return mr_president
        else: return ""


# run module from console
if __name__ == "__main__":
        if len(sys.argv) != 2:
                print "usage: " + sys.argv[0] + " 27th"
                sys.exit(0)
        else:
                number = (sys.argv[1])
                print pres(number)
