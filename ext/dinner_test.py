#!/usr/bin/python
from .. import aiml
import sys
import dinner

# setup bot
k = aiml.Kernel()
k.learn("../myaiml/dinner.aiml")

# verbose: displays error messages to console
k.verbose(True)

while True:
    query = raw_input("> ") 
    resp = k.respond(query)        
    if resp == "":
        print resp
    else:
        print dinner.dinner(resp)

