#!/usr/bin/python
import json
import requests
import sys

url = "http://api.aerisapi.com/observations/%s?client_id=%s&client_secret=%s"

client_id = "wNqk1Xks2XPVwF3a9zOwx"
client_secret = "nfcGKtQVOJCSP8JdeIkLlmVLoik8BfZkSVQ4BqPb"

def weather(place):
    # parse place string if not auto
    place = place.strip().lower()
    if place != ":auto":
        parts = place.split(',')
        if len(parts) == 1:
            # has no commas
            parts = place.split(' ')
            half = len(parts)/2
            place = ""
            place = "+".join(parts[0:half]) + ',' + "+".join(parts[half:])
    place = place.replace(" ", "+")

    # get the data
    r = requests.get(url % (place, client_id, client_secret)).json()

    if r['success']:
        ob = r['response']['ob']
        # figure out if it is sunny/cloudy/raining
        weath = ob['weather'].lower()
        if "sun" in weath: weath = "s"
        elif "rain" in weath: weath = "r"
        elif "cloud" in weath: weath = "c"
        else: weath = "s" # this default is probably bad...
        return "weather %s %d f" % (weath, ob['tempF'])
    else:
        return "Please be more specific with your place."

if __name__ == "__main__":
    print weather(" ".join(sys.argv[1:]))

