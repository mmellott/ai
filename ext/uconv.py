#!/usr/bin/python
################################################################################
# FILE: uconv.py
# AUTHOR: Greg Humphrey (greg.humphrey@asu.edu)
# DESCRIPTION:
#	The fn uconv returns the unit conversion by sending a GET request to
#       the Google and scraping the results. Probably should have named this
#       a little better.
#
################################################################################
import requests # "urllib2 sucks giant horse...balloons." - Matthew Mellott
import sys
import re
from bs4 import BeautifulSoup

# this is the unit conversion server I'm abusing
url = "http://www.onlineconversion.com/auto_convert.pl"

# returns unit conversion
def uconv(conversion):
	# format input for req	
        conversion = re.sub(r' a ', " ", conversion)
        conversion = re.sub(r' an ', " ", conversion)
	#print "conversion: " + conversion

        payload = {'have':conversion}
        soup = BeautifulSoup(requests.post(url, data=payload).text)
        converted = soup.find("tt")
        #print "converted: " + converted.text
        
        if converted:
            if converted.text == "m^3":
                return ""
            else:
                return re.sub(r'  ', " ", converted.text)
        else:
            return ""


# run module from console
if __name__ == "__main__":
	if len(sys.argv) != 2:
		print "usage: " + sys.argv[0] + " \"convert 4 teaspoons to a cup\""
		sys.exit(0)
	else:
		conversion = sys.argv[1]
                #print "conversion: " + conversion
		print uconv(conversion)
