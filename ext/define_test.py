#!/usr/bin/python
import aiml
import sys
import define

# setup bot
k = aiml.Kernel()
k.learn("myaiml/define.aiml")

# verbose: displays error messages to console
k.verbose(True)

while True:
	query = raw_input("> ") 
	resp = k.respond(query)        
        if resp == "":
            print resp
        else:
            print define.define(resp)

