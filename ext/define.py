#!/usr/bin/python
################################################################################
# FILE: gtime.py
# AUTHOR: Matthew Mellott (mmellott@asu.edu)
# DESCRIPTION:
#	The fn gtime retuns the current time in city by sending a GET request
#	to a website. The lib bs4 is then used to parse a time value from
#	the html in the response.
#
################################################################################
import requests # urllib2 sucks giant horse...balloons.
import sys
import re # OH GAWD WHY

from bs4 import BeautifulSoup

# dictionary website
url = "http://www.yourdictionary.com/"


# returns first definition of the term  
def define(term):

	# get the page and turn it into soup
	page = requests.get(url + term).text
        #print page
        #f = open('file.txt', 'w')
        #f.write(page)
	#f.close()
        soup = BeautifulSoup(page)
	soupylist = soup.find_all("meta")
         
       # for i in xrange(0, len(soupylist)):
       #     print "\n\n"
       #     print i
       #     print soupylist[i]
        
        temp2 = "Word not found."        
        hasNumber = False
        
   
        if(str(soupylist[1]).startswith('<meta content="' + term)):
            temp = str(soupylist[1]).split(' ')
            temp2 = ''
            for n in temp[3:]:
                if re.match('[0-9]+', n) is None:
                    if re.match('.*\.',n) is not None:
                        temp2 += n + ' '
                        break
                    else:
                        temp2 += n + ' ' 
                else:
                    hasNumber = True
        
        
        # do some string manip
        if hasNumber:
            if re.match(".*(noun|adjective|verb)", temp2, flags=0) is not None:
                matchObj = (re.search("(noun|adjective|verb)", temp2))
                temp2 = temp2[matchObj.end():]
                
        parts = temp2.strip().split("...")
        if len(parts) > 1:
            temp2 = parts[0]
            parts = temp2.split(" ")
            parts[0] = parts[0].title()
            temp2 = " ".join(parts)
            return temp2 + "."    
        else:
            temp2 = temp2.lstrip()
            parts = temp2.split(" ")
            parts[0] = parts[0].title()
            temp2 = " ".join(parts)
            return temp2

	# if len(items): return items[0].text
	# else: return ""

# run module from console
if __name__ == "__main__":
	if len(sys.argv) != 2:
		print "usage: " + sys.argv[0] + " term "
		sys.exit(0)
	else:
            term = sys.argv[1]
            print define(term)

