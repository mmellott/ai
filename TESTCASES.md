who was the 27th president of the united states
say William Howard Taft

how many cups are in a gallon
say  gallon = 16 cups

what should i have for dinner
say {some kind of dinner food...}

what's the weather like in paderborn, germany
weather s 43 f {something like this}

what time is it in paderborn germany
time +2

what time is it in tempe, az
time -7

what time is it
time -7

define bloviate
say The definition of bloviate is to speak for a long time in an arrogant way.

define bromopnea
say Word not found.

how many days until june 5th 2013
say 37 days {or thereabouts}

how many days until june 5th
say 37 days {or thereabouts}
