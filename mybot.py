#!/usr/bin/python
import aiml

class MyBot:
    """ The mybot class implements respones based upon custom AIML files and
    feature fn's. """

    def __init__(self, aimlf, callme, app=""):
        """ Bot learns patters from aimlf and will call callme when respond is
        called. The app param appended front of responses. """
        self._k = aiml.Kernel()
        self._k.learn(aimlf)
        self._k.verbose(True)
        self._callme = callme
        self._app = app


    def respond(self, msg):
        """ Respond to msg by calling k.respond and passing that to
        self._callme fn if resp is not "". """
        match = self._k.respond(msg)
        if match != "":
            resp = self._callme(match)
            if resp != "":
                return self._app + resp
        return ""

