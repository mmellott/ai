# Do Not Read This

## Before using run.py
The fifo net scripts are included as a git submodule. Make sure to do this
before trying to use run.py script:

    git submodule init

If you want to update/modify the submodule cd into the dir and proceed with
your normal git workflow.

## New Extension Instructions

### So you want to add your own feature?
0. Try not to add any files in root dir.
1. You DO NOT need to make changes to std-startup.xml
2. An extension with name myext should provide the following files:

    - myaiml/myext.aiml
    - ext/myext.py
    - ext/myext_test.py

3. In myext.py, a funtion should be defined with the convention:

    def myext(msg):

4. Cheerios.
5. Doc strings might be cool. Or not...

### Plan?
Implement as many extensions as we can.

## Features (this needs to be updated ... )
### New
Alice can to respond to time questions. See examples below:

	time tempe
	time in paris
	what time is it in paderborn, germany
	what time is it

### Other
Alice can still answer all the other silly questions she could before.

# How to Run
### Step 0: Run Linux
The step is not very well defined. Some version of some kind of linux
with python \(version > 2.6?\) installed.

### Step 1: Clone
Just clone the repo to your computer.

### Step 2: Install `pip`
The program `pip` manages python packages.
On Ubuntu, installing `pip` should be easy \(I have not tried this, however\): 

	sudo apt-get install pip

If you want to install `pip` on CentOS...there are a few other steps.

### Step 3: Resolve Dependencies
Once you have `pip`, you just need to install two python modules: beautifulsoup4, requests.
Should be as simple as this:

	sudo pip install beautifulsoup4
	sudo pip install requests

### Step 4: Done
Sit back and enjoy. You need to invoke `run.py` to get things going. Run it without args
to see usage.
