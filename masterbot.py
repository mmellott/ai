#!/usr/bin/python
""" This file contains the MasterBot class. You can also run this as a main
module to test out the fn's that have been merged. """
import sys

import aiml
from mybot import MyBot
from ext import pres
from ext import uconv
from ext import dinner
from ext import weather
from ext import gtime
from ext import define
from ext import timediff

class MasterBot:
    """ Manages all bots. """
    
    def __init__(self):
        """ Init all of the bots that MasterBot contains. """
        # default aiml bot
        self._k = aiml.Kernel()
        self._k.verbose(True)
        self._k.learn("std-startup.xml")
        self._k.respond("load aiml b")

        # list other extension bots
        self._bot_l = []
        
        # add new features here
        self._add_feature("myaiml/pres.aiml", pres.pres, "say ")
        self._add_feature("myaiml/uconv.aiml", uconv.uconv, "say ")
        self._add_feature("myaiml/dinner.aiml", dinner.dinner, "say ")
        self._add_feature("myaiml/weather.aiml", weather.weather)
        self._add_feature("myaiml/gtime.aiml", gtime.gtime)
        self._add_feature("myaiml/define.aiml", define.define, "say ")
        self._add_feature("myaiml/timediff.aiml", timediff.timediff, "say ")


    def _add_feature(self, aiml_fname, fn, app=""):
        """ Add feature to bot. """
        self._bot_l.append(MyBot(aiml_fname, fn, app))


    def respond(self, msg):
        """ Respond to input msg by checking extensions first and then checking
        std AIML bot. """
        # check extensions
        resp = ""
        for bot in self._bot_l:
            try: # just in case extension bot throws exception
                resp = bot.respond(msg)
                if resp != "": break 
            except:
                e = sys.exc_info()[0]  
                sys.stderr.write(str(e) + '\n')

        # feed std bot msg to build session even if we are not going to use it
        kresp = self._k.respond(msg)

        # pick resp
        if resp != "": return resp
        else: return "say " + kresp

if __name__ == "__main__":
    b = MasterBot()
    print "use /q to quit and /r to reload bot"

    while True:
        msg = raw_input("> ")
        if msg == "/q": sys.exit("see you later gator")
        if msg == "/r": b = MasterBot()
        else: print b.respond(msg)
